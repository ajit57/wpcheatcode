<?php


//custom post register
add_action( 'init', 'foodon_theme_restaurant' );
function foodon_theme_restaurant() {
	register_post_type( 'restaurant',
		array(
			'labels' => array(
				'name' => __( 'Restaurants' ),
				'singular_name' => __( 'Restaurant' ),
				'add_new_item' => __( 'Add New Restaurant' ),
                
			),
			'public' => true,
            'publicly_queryable' => true,
            'has_archive'        => true,
            'hierarchical'       => false,
			'supports' => array('title', 'editor', 'thumbnail', 'excerpt', 'page-attributes', 'custom-fields'),
		)
	);
	
}


//custom post loop 
<?php
 global $post;
 $args= array(
        "post_type" => 'restaurant',
        "posts_per_page" => '-1',
        "orderby" => 'menu_order',
        "order" => 'DESC'
 );
 $my_posts = get_posts( $args );
 foreach($my_posts as $post) : setup_postdata($post);
 ?>
 <?php
 $btn_text = get_post_meta($post->ID, 'restaurant_btn_text', true);
 ?>
   <div class="col-md-4 col-sm-4">
    <div class="single_featured_restaurant">
        <div class="featured_logo">
           <span class="middle_align"></span>
            <?php the_post_thumbnail(); ?>
        </div>
        <h2><?php the_title(); ?></h2>
        <?php the_excerpt(); ?>
        <a class="box_btn" href="<?php the_permalink(); ?>"><?php echo $btn_text; ?></a>
    </div>
</div>
<?php endforeach; ?>  


//wordpress dynamic menu register

function your_theme_supports(){
    
    load_theme_textdomain( 'foodon', get_template_directory() . '/languages' );
    
    add_theme_support( 'post-thumbnails' );
    add_image_size( 'post-image', 400, 400, true );
    
    register_nav_menus( array(
		'main'   => __( 'Main menu', 'foodon' ),
	) ); 
}
endif;
add_action('after_setup_theme', 'your_theme_supports');


//uses of dynamic nav menu
<?php wp_nav_menu(array('theme_location' => 'main', 'menu_id' => 'nav', 'fallback_cb' => 'wp_page_menu', )); ?>





















?>